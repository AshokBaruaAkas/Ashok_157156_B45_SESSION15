<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Information and Marks Collection Form</title>
    <link rel="stylesheet" href="../../src/BITM/SEIP_157156/style.css">
</head>
<body>
    <form action="process.php" method="post">
        <h2>Student Information and Marks Collection Form</h2>
        <hr>
        <div class="studentInformation">
            <p>Student Name:</p><input type="text" name="studentName">

            <p>Student ID:</p><input type="number" name="studentID">
        </div>
        <div class="marks">
            <p>Masks Obtained in Bangla:</p><input type="number" name="banglaMark" min="1" max="100">

            <p>Masks Obtained in English:</p><input type="number" name="englishMark" min="1" max="100">

            <p>Masks Obtained in Math:</p><input type="number" name="mathMark" min="1" max="100">
        </div>
        <input type="submit" value="Submit">
    </form>
</body>
</html>