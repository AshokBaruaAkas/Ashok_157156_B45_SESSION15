<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Result With Grade</title>
    <link rel="stylesheet" href="../../src/BITM/SEIP_157156/style.css">
</head>
<body>
    <?php
        require_once("../../vendor/autoload.php");

        //set property for Student.php
        $objectStudent = new \App\Student();

        $objectStudent->setStudentName($_POST['studentName']);
        $objectStudent->setStudentID($_POST['studentID']);

        //set property for Course.php
        $objectCourse = new \App\Course();

        $objectCourse->setBanglaMark($_POST['banglaMark']);
        $objectCourse->setEnglishMark($_POST['englishMark']);
        $objectCourse->setMathMark($_POST['mathMark']);

        //getting Student Information from Student.php
        $studentName = $objectStudent->getStudentName();
        $studentID = $objectStudent->getStudentID();

        //getting grade and Mark from Course.php
        $banglaGrade = $objectCourse->getBanglaGrade();
        $englishGrade = $objectCourse->getEnglishGrade();
        $mathGrade = $objectCourse->getMathGrade();
        $banglaMark = $objectCourse->getBanglaMark();
        $englishMark = $objectCourse->getEnglishMark();
        $mathMark = $objectCourse->getMathMark();

        $result = <<<RESULT
<div class='resultSheet'>
    <table>
        <tr>
            <td colspan='2'><h2>Result Sheet</h2><hr></td>
        </tr>
        <tr>
            <td>Student Name:</td>
            <td>$studentName</td>
        </tr>
        <tr>
            <td>Student ID:</td>
            <td>$studentID</td>
        </tr>
    </table>
    <table>
        <tr>
            <th>Subject</th>
            <th>Mark</th>
            <th>Grade</th>
        </tr>
        <tr>
            <td>Bangla</td>
            <td>$banglaMark</td>
            <td>$banglaGrade</td>
        </tr>
        <tr>
            <td>English</td>
            <td>$englishMark</td>
            <td>$englishGrade</td>
        </tr>
        <tr>
            <td>Math</td>
            <td>$mathMark</td>
            <td>$mathGrade</td>
        </tr>
    </table>
</div>
<br><br>
RESULT;

    echo $result;
    file_put_contents("result.html",$result,FILE_APPEND);
    ?>
</body>
</html>