<?php

namespace App;

class Course
{
    private $banglaMark;
    private $banglaGrade;

    private $englishMark;
    private $englishGrade;

    private $mathMark;
    private $mathGrade;

    //set all property
    public function setBanglaMark($banglaMark)
    {
        $this->banglaMark = $banglaMark;
    }

    public function setBanglaGrade($banglaMark)
    {
        return $this->banglaGrade = $this->mark2Grade($banglaMark);
    }

    public function setEnglishMark($englishMark)
    {
        $this->englishMark = $englishMark;
    }

    public function setEnglishGrade($englishMark)
    {
        return $this->englishGrade = $this->mark2Grade($englishMark);
    }

    public function setMathMark($mathMark)
    {
        $this->mathMark = $mathMark;
    }

    public function setMathGrade($mathMark)
    {
        return $this->mathGrade = $this->mark2Grade($mathMark);
    }
    //get all property
    public function getBanglaMark()
    {
        return $this->banglaMark;
    }

    public function getBanglaGrade()
    {

        return $this->banglaGrade = $this->setBanglaGrade($this->banglaMark);
    }

    public function getEnglishMark()
    {
        return $this->englishMark;
    }

    public function getEnglishGrade()
    {
        return $this->englishGrade = $this->setEnglishGrade($this->englishMark);
    }

    public function getMathMark()
    {
        return $this->mathMark;
    }

    public function getMathGrade()
    {
        return $this->mathGrade = $this->setMathGrade($this->mathMark);
    }

    //needed Function Here
    public function mark2Grade($mark){
        switch($mark){
            case ($mark>79):
                $Grade = "A+";
                break;
            case ($mark>69):
                $Grade = "A";
                break;
            case ($mark>59):
                $Grade = "A-";
                break;
            case ($mark>49):
                $Grade = "B";
                break;
            case ($mark>39):
                $Grade = "C";
                break;
            case ($mark>32):
                $Grade = "D";
                break;
            default:
                $Grade = "F";
        }
        return $Grade;
    }
}