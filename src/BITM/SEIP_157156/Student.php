<?php

namespace App;

class Student
{
    private $studentName;
    private $studentID;

    public function setStudentName($studentName)
    {
        $this->studentName = $studentName;
    }

    public function getStudentName()
    {
        return $this->studentName;
    }

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}